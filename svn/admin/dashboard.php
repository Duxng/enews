<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dashborad</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="css/dashboard.css">
</head>

<body>
    <div class="sidebar" style="min-height: 603px;">
        <ul class="sidebar-list">
            <li class="sidebar-item">
                <a href="../admincp/admin_list.php"><span>Quản Lý Admin</span></a>
            </li>
            <li class="sidebar-item">
                <a href="../categories/category_list.php"><span>Quản Lý Danh Mục</span></a>
            </li>
            <li class="sidebar-item">
                <a href="../article/article_list.php"><span> Quản Lý Bài Viết</span></a>
            </li>
            <li class="sidebar-item">
                <a href="../role/role_list.php"><span>Quản lý Role</span></a>
            </li>
            <li class="sidebar-item">
                <a href="../logout.php"><span>Log Out</span></a>
            </li>
        </ul>
    </div>
</body>

</html>