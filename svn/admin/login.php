<?php
include('operate.php');
$errors = array();
if (isset($_POST['login'])) {
    $name = $_POST['admin_name'];
    $password = $_POST['password'];
    if (empty($name)) {
        $errors['name'] = "Admin Name is required.";
    }
    if (empty($password)) {
        $errors['password'] = "Password is required.";
    }
    if (empty($errors)) {
        $sql = "SELECT name,password,role FROM admin WHERE name='$name' AND password='$password'";
        $result = mysqli_query($con, $sql);
        $count = mysqli_num_rows($result);
        $checkrole = mysqli_fetch_assoc($result);
        if ($count > 0) {
            $_SESSION['login'] = $name;
            header('location: admincp/admin_list.php');
            $_SESSION['role'] = $checkrole['role'];
        } else {
            header('location: login.php');
        }
    }
}
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="css/adminstyle.css">
</head>

<body>
    <div class="container wrapper">
        <div class="row">
            <div class="col-6 image">
                <img src="../user/images/background.jpg" alt="" height="100%" width="100%">
            </div>
            <div class="col-6 login-form ">
                <div class="text-center">
                    <h4 class="mb-4">ADMIN LOGIN</h2>
                </div>
                <form method="POST" autocomplete="off" class="login-form-admin">
                    <div class="mt-4 form-group">
                        <input class="form-control form-control-user" name="admin_name" type="text" placeholder="Admin Name">
                        <?php
                        if (!empty($errors['name'])) : ?>
                            <div class="text-danger"><?php echo $errors['name']; ?></div>
                        <?php endif; ?>
                    </div>
                    <div class="mt-4 form-group">
                        <input class="form-control form-control-user" name="password" type="password" placeholder="Password">
                        <?php if (!empty($errors['password'])) : ?>
                            <div class="text-danger"><?php echo $errors['password']; ?></div>
                        <?php endif; ?>
                    </div>

                    <input type="submit" name="login" class=" mt-4 btn btn-primary btn-login" value="Login"></input>
                </form>
            </div>

        </div>

    </div>
</body>

</html>