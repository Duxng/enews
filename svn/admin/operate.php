<?php
include('config/config.php');
session_start();

function checkResult($result)
{
    global $con;
    if (isset($result)) {
        return $result;
    } else {
        echo mysqli_error($con);
        exit;
    }
}

function add_Role($data)
{
    global $con;
    $name = $data['name'];
    $sql = "INSERT INTO `roles`(`name`,`created_at`, `updated_at`) VALUES ('$name', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)";
    $result = mysqli_query($con, $sql);
    if (checkResult($result)) {
        redirect('role_list.php');
    }
}

function get_list_Role()
{
    global $con;
    $sql = "SELECT * FROM `roles`";
    $result = mysqli_query($con, $sql);
    return checkResult($result);
}

function get_id_Role($id)
{
    global $con;
    $sql = "SELECT * FROM `roles` where role_id ='$id' ";
    $result = mysqli_query($con, $sql);
    checkResult($result);
    return mysqli_fetch_assoc($result);
}

function update_role($data)
{
    global $con;
    $role_id = $data['role_id'];
    $name = $data['name'];

    $sql = "UPDATE `roles` SET `name`='$name', `updated_at`=CURRENT_TIMESTAMP WHERE `role_id`='$role_id'";
    $result = mysqli_query($con, $sql);
    if (checkresult($result)) {
        redirect('role_list.php');
    }
}

function delete_role($id)
{
    global $con;
    $sql = "DELETE FROM `roles` WHERE role_id = '$id'";
    $result = mysqli_query($con, $sql);
    return checkResult($result);
}

function add_Admin($data)
{
    global $con;
    $name = $data['name'];
    $password = $data['password'];
    $role = $data['role_id'];
    $status = 1;
    $sql = "INSERT INTO `admin`(
        `name`,
        `password`,
        `role`,
        `status`
    ) 
    VALUES ('$name', '$password','$role', '$status')";
    print_r($sql);
    $result = mysqli_query($con, $sql);
    if (checkResult($result)) {
        redirect('admin_list.php');
    }
}

function get_list_Admin()
{
    global $con;
    $sql = "SELECT * FROM `admin`";
    $result = mysqli_query($con, $sql);
    return checkResult($result);
}

function get_id_Admin($id)
{
    global $con;
    $sql = "SELECT * FROM `admin` where admin_id ='$id' ";
    $result = mysqli_query($con, $sql);
    checkResult($result);
    return mysqli_fetch_assoc($result);
}

function update_admin($data)
{
    global $con;
    $admin_id = $data['admin_id'];
    $name = $data['name'];
    $password = $data['password'];
    $role = $data['role_id'];
    $status = $data['status'];

    $sql = "UPDATE `admin` SET `name`='$name', `password`='$password', `role`='$role', `status`='$status', `updated_at`=CURRENT_TIMESTAMP  WHERE `admin_id`='$admin_id'";
    $result = mysqli_query($con, $sql);
    if (checkresult($result)) {
        redirect('admin_list.php');
    }
}
function delete_admin($id)
{
    global $con;
    $sql = "DELETE FROM `admin` WHERE admin_id = '$id'";
    $result = mysqli_query($con, $sql);
    if (checkresult($result)) {
        redirect('admin_list.php');
    }
}

function add_Categories($data)
{
    global $con;
    $name = mysqli_real_escape_string($con, $data['name']);
    $checkSql = "SELECT COUNT(*) AS category_count FROM categories WHERE name = '$name'";
    $checkResult = mysqli_query($con, $checkSql);
    $categoryCount = mysqli_fetch_assoc($checkResult)['category_count'];

    if ($categoryCount > 0) {
        $_SESSION['error_message'] = "Category name already exists.";
    } else {
        $sql = "INSERT INTO categories (name, created_at, updated_at) 
                VALUES ('$name', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)";
        $result = mysqli_query($con, $sql);
        if ($result) {
            $_SESSION['success_message'] = "Category added successfully.";
        } else {
            $_SESSION['error_message'] = "An error occurred while adding the category.";
        }
    }

    redirect('category_list.php');
}

function get_list_Categories()
{
    global $con;
    $sql = "SELECT * FROM `categories`";
    $result = mysqli_query($con, $sql);
    return checkResult($result);
}

function get_id_Categories($id)
{
    global $con;
    $sql = "SELECT * FROM `categories` where categories_id ='$id' ";
    $result = mysqli_query($con, $sql);
    checkResult($result);
    return mysqli_fetch_assoc($result);
}

function update_Categories($data)
{
    global $con;
    $categories_id = $data['categories_id'];
    $name = $data['name'];
    $sql = "UPDATE `categories` SET `name`='$name',`updated_at`=CURRENT_TIMESTAMP WHERE `categories_id`='$categories_id'";
    $result = mysqli_query($con, $sql);
    if (checkresult($result)) {
        redirect('category_list.php');
    }
}
function delete_Categories($id)
{
    global $con;
    $sql = "DELETE FROM `categories` WHERE categories_id = '$id'";
    $result = mysqli_query($con, $sql);
    if (checkresult($result)) {
        redirect('category_list.php');
    }
}

function add_Article($data, $picture)
{
    global $con;
    $title = $data['title'];
    $content = mysqli_real_escape_string($con, $data['content']);
    $picture = $picture;
    $description = $data['description'];
    $categories_id = $data['categories_id'];
    $sql = "INSERT INTO articles ";
    $sql .= "(title, content,picture, description, categories_id, created_at, updated_at) ";
    $sql .= "VALUES (";
    $sql .= "'" . $title . "',";
    $sql .= "'" . $content . "',";
    $sql .= "'" . $picture . "',";
    $sql .= "'" . $description . "',";
    $sql .= "'" . $categories_id . "',";
    $sql .= 'CURRENT_TIMESTAMP' . ",";
    $sql .= 'CURRENT_TIMESTAMP';
    $sql .= ")";
    $result = mysqli_query($con, $sql);
    if (checkResult($result)) {
        redirect('article_list.php');
    }
}

function get_list_Article($start, $limit)
{
    global $con;
    $sql = "SELECT * FROM articles ORDER BY updated_at ASC LIMIT $start, $limit";
    $result = mysqli_query($con, $sql);
    return checkResult($result);
}

function get_total_Article()
{
    global $con;
    $sql = "SELECT COUNT(*) AS total FROM articles";
    $result = mysqli_query($con, $sql);
    $data = mysqli_fetch_assoc($result);
    return $data['total'];
}

function get_id_Article($id)
{
    global $con;
    $sql = "SELECT * FROM `articles` where article_id ='$id' ";
    $result = mysqli_query($con, $sql);
    checkResult($result);
    return mysqli_fetch_assoc($result);
}

function get_detail_Article($id)
{
    global $con;
    $sql = "SELECT article_id, content, created_at, updated_at FROM `articles` WHERE article_id='$id'";
    $result = mysqli_query($con, $sql);
    checkResult($result);
    return mysqli_fetch_assoc($result);
}

function update_Article($data, $picture)
{
    global $con;
    $article_id = $data['article_id'];
    $title = mysqli_real_escape_string($con, $data['title']);
    $description = mysqli_real_escape_string($con, $data['description']);
    $content = mysqli_real_escape_string($con, $data['content']);
    $categories_id = $data['categories_id'];
    $sql = "UPDATE `articles` SET `title`='$title', `description`='$description', `content`='$content', `picture`='$picture', `categories_id`='$categories_id', `updated_at`=CURRENT_TIMESTAMP WHERE `article_id`='$article_id'";
    $result = mysqli_query($con, $sql);
    if (checkResult($result)) {
        unset($_SESSION['picture_article']);
        redirect('article_list.php');
    } else {
        echo "Update failed: " . mysqli_error($con);
    }
}

function delete_Article($id)
{
    global $con;
    $sql = "DELETE FROM `articles` WHERE article_id = '$id'";
    $result = mysqli_query($con, $sql);
    if (checkresult($result)) {
        redirect('article_list.php');
    }
}

function get_total_articles()
{
    global $con;
    $sql = "SELECT COUNT(*) AS total FROM articles";
    $result = mysqli_query($con, $sql);
    $row = mysqli_fetch_assoc($result);
    return $row['total'];
}


function get_all_roles()
{
    global $con;
    $sql = "SELECT name FROM `roles`";
    $result = mysqli_query($con, $sql);
    if (checkresult($result)) {
        return $result;
    }
}

function get_all_categories()
{
    global $con;
    $sql = "SELECT name FROM `categories`";
    $result = mysqli_query($con, $sql);
    if (checkresult($result)) {
        return $result;
    }
}

function get_all_admin()
{
    global $con;
    $sql = "SELECT name FROM `admin`";
    $result = mysqli_query($con, $sql);
    if (checkResult($result)) {
        return $result;
    }
}

function perform_search($query)
{
    global $con;
    $sql = "SELECT * FROM articles WHERE title LIKE '%$query%' OR description LIKE '%$query%'";
    $result = mysqli_query($con, $sql);

    return $result;
}

function search_by_category($categories_id, $start, $limit)
{
    global $con;

    $categories_id = mysqli_real_escape_string($con, $categories_id);

    $sql = "SELECT * FROM articles WHERE categories_id = '$categories_id' ORDER BY updated_at DESC LIMIT $start, $limit";
    $result = mysqli_query($con, $sql);

    return checkResult($result);
}
