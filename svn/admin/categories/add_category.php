<?php
include('../operate.php');

if (!isset($_SESSION['login'])) {
    header('location: ../login.php');
    exit;
}

$errors = array();
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (empty($_POST['name'])) {
        $errors['name'] = "Không được để trống danh mục";
    } elseif (!preg_match('/^[\p{L}\s\d]+$/u', $_POST['name'])) {
        $errors['name'] = 'Tên danh mục chỉ được nhập chữ cái, khoảng trắng và số.';
    }
    $existingCategory = get_all_categories();
    $newCategory = $_POST['name'];

    foreach ($existingCategory as $existingCategory) {
        if (strtolower($existingCategory['name']) === strtolower($newCategory)) {
            $errors['name'] = 'Danh mục đã tồn tại.';
            break;
        }
    }
    if (empty($errors)) {
        add_Categories($_POST);
    }
}

?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Create New Role</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/dashboard.css">
</head>

<body>
    <div class="container-fluid px-0 h-100vh row mt-5">
        <div class="col-2" style="height: auto; background-color: rgba(113, 99, 186, 255);">
            <?php include('../dashboard.php') ?>
        </div>
        <div class="col-10">
            <button class="btn btn-primary mb-2"><a href="category_list.php" class="text-light">Back to Index</a></button>
            <form method="POST" action="<?php echo $_SERVER['PHP_SELF'] ?>">
                <div class="mb-3">
                    <label>Category</label>
                    <input type="text" class="form-control" placeholder="Enter new category" name="name" value="<?php echo  isset($_POST['name']) ? $_POST['name'] : "" ?>" autocomplete="off">
                    <?php if (!empty($errors['name'])) : ?>
                        <div class="text-danger"><?php echo $errors['name']; ?></div>
                    <?php endif; ?>
                </div>
                <button type="submit" class="btn btn-primary" name="submit">Submit</button>
            </form>
        </div>

    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script>
</body>

</html>