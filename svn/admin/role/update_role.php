<?php
include('../operate.php');

if (!isset($_SESSION['login'])) {
    header('location: ../login.php');
    exit;
}

$errors = array();
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (empty($_POST['name'])) {
        $errors['name'] = "Không được để trống role";
    } elseif (!preg_match('/^[\p{L}\s]+$/u', $_POST['name'])) {
        $errors['name'] = 'Tên role chỉ được nhập chữ cái và khoảng trắng.';
    }
    if (empty($errors)) {
        update_role($_POST);
    }
} else {
    if (isset($_GET['update_id'])) {
        $role_id = $_GET['update_id'];
        $role = get_id_Role($role_id);
    }
}
?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Update Role</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/dashboard.css">
</head>


<body>
    <div class="container-fluid px-0 h-100vh row mt-5">
        <div class="col-2">
            <?php include('../dashboard.php') ?>
        </div>
        <div class="col-10">
            <button class="btn btn-primary mb-2"><a href="role_list.php" class="text-light">Back to Index</a></button>
            <form method="POST" action="<?php echo $_SERVER['PHP_SELF'] ?>">
                <div class="mb-3">
                    <label>ID</label>
                    <input type="text" class="form-control" name="role_id" autocomplete="off" value="<?php echo (isset($role) && isset($role['role_id'])) ? $role['role_id'] : $_POST['role_id']  ?>" readonly>
                </div>
                <div class="mb-3">
                    <label>Role</label>
                    <input type="text" class="form-control" name="name" autocomplete="off" value="<?php echo (isset($role) && isset($role['name'])) ? $role['name'] : $_POST['name']; ?>">
                    <?php if (!empty($errors['name'])) : ?>
                        <div class="text-danger"><?php echo $errors['name']; ?></div>
                    <?php endif; ?>
                </div>
                <button type="submit" class="btn btn-primary" name="submit">Update</button>
            </form>
        </div>

    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script>
</body>

</html>

</html>