<?php
include('../operate.php');

if (!isset($_SESSION['login'])) {
    header('location: ../login.php');
    exit;
}

?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin list</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/dashboard.css">
</head>

<body>
    <div class="container-fluid px-0 mt-5 row">
        <div class="col-2" style="height: auto; background-color: rgba(113, 99, 186, 255);">
            <?php include('../dashboard.php') ?>
        </div>
        <div class="col-10">
            <button class="btn btn-primary mb-2"><a href="article_list.php" class="text-light">Back to Index</a></button>
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">Article_ID</th>
                        <th scope="col">Content</th>
                        <th scope="col">Created_at</th>
                        <th scope="col">Updated_at</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (isset($_GET['content_id'])) {
                        $content_id = $_GET['content_id'];
                        $content = get_detail_Article($content_id);
                    }
                    ?>
                    <tr>
                        <td><?php echo $content['article_id']; ?></th>
                        <td><?php echo $content['content']; ?></td>
                        <td><?php echo $content['created_at']; ?></td>
                        <td><?php echo $content['updated_at'] ?></td>
                    </tr>
                </tbody>
            </table>
        </div>

    </div>
</body>

</html>