<?php
include('../operate.php');

if (!isset($_SESSION['login'])) {
    header('location: ../login.php');
    exit;
}

// if ($_SESSION['role'] != 2) {
//     header('location: ../role/role_list.php');
// }

$limit = 10;
$page = isset($_GET['page']) ? $_GET['page'] : 1;
$start = ($page - 1) * $limit;
$total_records = get_total_Article();
$total_pages = ceil($total_records / $limit);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin list</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/dashboard.css">
</head>

<body>
    <div class="container-fluid px-0 mt-5 row">
        <div class="col-2" style="height: auto; background-color: rgba(113, 99, 186, 255);">
            <?php include('../dashboard.php') ?>
        </div>
        <div class="col-10">
            <div style="display:flex ;justify-content:space-between">
                <button class="btn btn-primary mb-2"><a href="add_article.php" class="text-light">Add Article</a></button>
                <!-- <form action="search_by_cate.php" method="get" class="">
                    <select style=" width:150px" class="form-select" name="categories_id">
                        <?php
                        $sql = "SELECT categories_id, name FROM `categories` ORDER BY categories_id ASC";
                        $result = mysqli_query($con, $sql);

                        while ($row = mysqli_fetch_assoc($result)) {
                            echo '<option value="' . $row['categories_id'] . '">' . $row['name'] . '</option>';
                        }
                        ?>
                    </select>
                    <button type="submit" name="search" class="btn btn-dark">Search</button>
                </form> -->
                <form action="search.php" method="get" class="ml-3">
                    <input type="text" name="search" placeholder="Tìm Kiếm" value="">
                    <button type="submit" class="btn btn-dark">Search</button>
                </form>
            </div>
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">Article_ID</th>
                        <th scope="col">Title</th>
                        <th scope="col">Description</th>
                        <th scope="col">Picture</th>
                        <th scope="col">Categories_ID</th>
                        <th scope="col">Created_at</th>
                        <th scope="col">Updated_at</th>
                        <th scope="col" style="width: 180px;">Edit</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $result = get_list_Article($start, $limit);
                    $link = '../uploads/';
                    while ($row = mysqli_fetch_assoc($result)) : ?>
                        <tr>
                            <td><?php echo $row['article_id']; ?></td>
                            <td class=""><?php echo $row['title']; ?></td>
                            <td class=""><a href="detail_article.php?content_id=<?php echo $row['article_id']; ?>"><?php echo $row['description']; ?></a></td>
                            <td style="display:flex"><img style="height:100px;width: 100px;margin:auto; object-fit: cover;" src="../uploads/<?php echo $row['picture'] ?>" alt=""></td>
                            <td style="text-align:center"><?php echo $row['categories_id']; ?></td>
                            <td><?php echo $row['created_at']; ?></td>
                            <td><?php echo $row['updated_at'] ?></td>
                            <td>
                                <button class="btn btn-primary"><a href="update_article.php?update_id=<?php echo $row['article_id']; ?>" class="text-light">Update</a></button>
                                <button class="btn btn-danger"><a href="delete_article.php?delete_id=<?php echo $row['article_id']; ?>" class="text-light">Delete</a></button>
                            </td>
                        </tr>
                    <?php endwhile; ?>
                </tbody>
            </table>
            <nav aria-label="Page navigation">
                <ul class="pagination">
                    <?php for ($i = 1; $i <= $total_pages; $i++) : ?>
                        <li class="page-item <?php echo ($i == $page) ? 'active' : ''; ?>">
                            <a class="page-link" href="?page=<?php echo $i; ?>"><?php echo $i; ?></a>
                        </li>
                    <?php endfor; ?>
                </ul>
            </nav>
        </div>
    </div>
</body>

</html>