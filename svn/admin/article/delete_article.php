<?php
include('../operate.php');

if (!isset($_SESSION['login'])) {
    header('location: ../login.php');
    exit;
}

if (isset($_GET['delete_id'])) {
    $article_id = $_GET['delete_id'];
    $article = get_id_Article($article_id);
}
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['delete_article'])) {
    delete_Article($_POST['article_id']);
}
$link = '../uploads/';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Detele Role</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/dashboard.css">
</head>

<body>
    <div class="container-fluid px-0 h-100vh row mt-5">
        <div class="col-2" style="height: auto; background-color: rgba(113, 99, 186, 255);">
            <?php include('../dashboard.php') ?>
        </div>
        <div class="col-10">
            <button class="btn btn-primary mb-2"><a href="article_list.php" class="text-light">Back to Index</a></button>
            <form method="POST" action="<?php echo $_SERVER['PHP_SELF'] ?>" enctype="multipart/form-data">
                <div class="mb-3">
                    <label>ID</label>
                    <input type="text" class="form-control" name="article_id" autocomplete="off" value="<?php echo $article['article_id'] ?>" readonly>
                </div>
                <div class="mb-3">
                    <label>Name</label>
                    <input type="text" class="form-control" name="title" autocomplete="off" value="<?php echo $article['title'] ?>" readonly>
                </div>
                <div class="mb-3">
                    <img style="height:100px;width: 100px" src="<?php echo $link . $article['picture'] ?>" class="card-img-top" alt="...">
                </div>
                <div class="mb-4">
                    <label for="exampleFormControlTextarea1" class="form-label">Content</label>
                    <textarea class="form-control" name="content" id="content_edit" rows="4" readonly>
                        <?php echo $article['content']; ?>
                    </textarea>
                </div>
                <div class="mb-3">
                    <label>Categories_ID</label>
                    <input type="text" class="form-control" name="categories_id" autocomplete="off" value="<?php echo $article['categories_id'] ?>" readonly>
                </div>

                <button type="submit" class="btn btn-primary" name="delete_article" onclick="return confirm('Are you sure you want to delete this article?');">Delete Article</button>
            </form>
        </div>

    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script>
    <script src="https://cdn.tiny.cloud/1/v2actx1pn02mabdp87lonhsovy7lb90aca7wfhf3149o0eoa/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
        tinymce.init({
            image_dimensions: false,
            image_class_list: [{
                title: 'Responsive',
                value: 'img-responsive'
            }],
            image_description: true,
            image_caption: true,
            height: "600",
            selector: 'textarea#content_edit',
            plugins: 'code table lists image code ',
            toolbar: 'link image | code |image |undo redo | formatselect| bold italic | alignleft aligncenter alignright | indent outdent | bullist numlist | code | table',
            image_title: true,
            automatic_uploads: true,
            file_picker_types: 'image',
            file_picker_callback: (cb, value, meta) => {
                const input = document.createElement('input');
                input.setAttribute('type', 'file');
                input.setAttribute('accept', 'image/*');
                input.addEventListener('change', (e) => {
                    const file = e.target.files[0];
                    const reader = new FileReader();
                    reader.addEventListener('load', () => {
                        const id = 'blobid' + (new Date()).getTime();
                        const blobCache = tinymce.activeEditor.editorUpload.blobCache;
                        const base64 = reader.result.split(',')[1];
                        const blobInfo = blobCache.create(id, file, base64);
                        blobCache.add(blobInfo);
                        cb(blobInfo.blobUri(), {
                            title: file.name
                        });
                    });
                    reader.readAsDataURL(file);
                });
                input.click();
            },
            content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:16px }'
        });
    </script>
</body>

</html>
</body>

</html>