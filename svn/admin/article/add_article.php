<?php
include('../operate.php');

if (!isset($_SESSION['login'])) {
    header('location: ../login.php');
    exit;
}

$errors = array();
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (empty($_POST['title'])) {
        $errors['title'] = "Không được để trống tên";
    }
    if (empty($_POST['content'])) {
        $errors['content'] = "Không được để trống content.";
    }
    if (empty($_FILES['picture']['name'])) {
        $errors['picture'] = "Hình ảnh không được để trống.";
    } elseif ($_FILES['picture']['error'] !== UPLOAD_ERR_OK) {
        $errors['picture'] = "Hình ảnh không hợp lệ.";
    }
    if (empty($_POST['description'])) {
        $errors['description'] = "Không được để trống description";
    }
    if (empty($errors) && $_SERVER['REQUEST_METHOD'] === "POST") {
        $time = time();
        $file_Name = $time . $_FILES['picture']['name'];
        $file_Links = '../uploads/' . $file_Name;
        move_uploaded_file($_FILES['picture']['tmp_name'], $file_Links);
        add_Article($_POST, $file_Name);
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/dashboard.css">
</head>

<body>
    <div class="container-fluid px-0 h-100vh row mt-5">
        <div class="col-2 h-100vh" style="height: auto; background-color: rgba(113, 99, 186, 255);">
            <?php include('../dashboard.php') ?>
        </div>
        <div class="col-10">
            <button class="btn btn-primary mb-2"><a href="article_list.php" class="text-light">Back to Index</a></button>
            <form method="POST" action="<?php echo $_SERVER['PHP_SELF'] ?>" enctype="multipart/form-data">
                <div class="mb-4">
                    <label>Tiêu Đề</label>
                    <input type="text" class="form-control" placeholder="Enter Title " name="title" autocomplete="off" value="<?php echo isset($_POST['title']) ? $_POST['title'] : "" ?>">
                    <?php if (!empty($errors['title'])) : ?>
                        <div class="text-danger"><?php echo $errors['title']; ?></div>
                    <?php endif; ?>
                </div>
                <label>Thể Loại</label>
                <select class="form-control mb-4" name="categories_id">
                    <?php
                    $sql = "SELECT categories_id, name FROM `categories` ORDER BY categories_id ASC";
                    $result = mysqli_query($con, $sql);

                    while ($row = mysqli_fetch_assoc($result)) {
                        echo '<option value="' . $row['categories_id'] . '">' . $row['name'] . '</option>';
                    }
                    ?>
                </select>
                <div class="mb-4">
                    <label>Mô tả</label>
                    <input type="text" class="form-control" placeholder="Enter Description " name="description" autocomplete="off" value="<?php echo isset($_POST['description']) ? $_POST['description'] : "" ?>">
                    <?php if (!empty($errors['description'])) : ?>
                        <div class="text-danger"><?php echo $errors['description']; ?></div>
                    <?php endif; ?>
                </div>
                <div class="mb-3">
                    <label for="formFile" class="form-label">Ảnh Minh Họa</label>
                    <input class="form-control" type="file" name="picture" id="formFile">
                    <?php if (!empty($errors['picture'])) : ?>
                        <div class="text-danger"><?php echo $errors['picture']; ?></div>
                    <?php endif; ?>
                </div>
                <div class="">
                    <label for="exampleFormControlTextarea1" class="form-label">Nội Dung</label>
                    <textarea class="form-control" name="content" style="height: 400px;" id="content_edit"></textarea>
                    <?php if (!empty($errors['content'])) : ?>
                        <div class="text-danger"><?php echo $errors['content']; ?></div>
                    <?php endif; ?>
                </div>
                <button type="submit" class="btn btn-primary mt-4" name="submit">Submit</button>
            </form>
        </div>

    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script>
    <script src="https://cdn.tiny.cloud/1/v2actx1pn02mabdp87lonhsovy7lb90aca7wfhf3149o0eoa/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
        tinymce.init({
            image_dimensions: false,
            image_class_list: [{
                title: 'Responsive',
                value: 'img-responsive'
            }],
            image_description: true,
            image_caption: true,
            height: "600",
            selector: 'textarea#content_edit',
            plugins: 'code table lists image code ',
            toolbar: 'link image | code |image |undo redo | formatselect| bold italic | alignleft aligncenter alignright | indent outdent | bullist numlist | code | table',
            image_title: true,
            automatic_uploads: true,
            file_picker_types: 'image',
            file_picker_callback: (cb, value, meta) => {
                const input = document.createElement('input');
                input.setAttribute('type', 'file');
                input.setAttribute('accept', 'image/*');
                input.addEventListener('change', (e) => {
                    const file = e.target.files[0];
                    const reader = new FileReader();
                    reader.addEventListener('load', () => {
                        const id = 'blobid' + (new Date()).getTime();
                        const blobCache = tinymce.activeEditor.editorUpload.blobCache;
                        const base64 = reader.result.split(',')[1];
                        const blobInfo = blobCache.create(id, file, base64);
                        blobCache.add(blobInfo);
                        cb(blobInfo.blobUri(), {
                            title: file.name
                        });
                    });
                    reader.readAsDataURL(file);
                });
                input.click();
            },
            content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:16px }'
        });
    </script>
</body>

</html>