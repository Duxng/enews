<?php
include('../operate.php');
$link = '../uploads/';
$dataGet = null;
$errors = array();

if (!isset($_SESSION['login'])) {
    header('location: ../login.php');
    exit;
}


function validated($data)
{
    global $errors;
    if (empty($data['title'])) {
        $errors['title'] = "Không được để trống tiêu đề";
    }
    if (empty($data['description'])) {
        $errors['description'] = "Không được để trống mô tả";
    }
    if (empty($data['content'])) {
        $errors['content'] = "Không được để trống nội dung";
    } elseif (strlen($data['content']) < 10) {
        $errors['content'] = "Nội dung phải có ít nhất 10 ký tự.";
    }
}

function checkValidate()
{
    global $errors;
    return count($errors) === 0;
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    validated($_POST);
    if (checkValidate()) {
        $data = [];
        $data['title'] = $_POST['title'];
        $data['description'] = $_POST['description'];
        $data['content'] = $_POST['content'];
        $data['categories_id'] = $_POST['categories_id'];
        $data['article_id'] = $_POST['article_id'];
        $time = time();
        $file_Name = null;
        if (!empty($_FILES['picture']['name'])) {
            $file_Name =  $time . $_FILES['picture']['name'];
            $file_Links = '../uploads/' . $file_Name;
            move_uploaded_file($_FILES['picture']['tmp_name'], $file_Links);
            if (isset($_SESSION['picture_article']) && file_exists($_SESSION['picture_article'])) {
                unlink($_SESSION['picture_article']);
            }
            $_SESSION['picture_article'] = $file_Links;
        } else {
            $file_Name = $_SESSION['picture_article'];
        }
        update_Article($data, $file_Name);
    }
} else {
    if (isset($_GET['update_id'])) {
        global $dataGet;
        $id = $_GET['update_id'];
        $dataGet = get_id_Article($id);
        if ($dataGet['picture']) {
            $_SESSION['picture_article'] = $dataGet['picture'];
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Update Admin</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/dashboard.css">
</head>

<body>
    <div class="container-fluid px-0 h-100vh row mt-5">
        <div class="col-2" style="height: auto; background-color: rgba(113, 99, 186, 255);">
            <?php include('../dashboard.php') ?>
        </div>
        <div class="col-10">
            <button class="btn btn-primary mb-2"><a href="article_list.php" class="text-light">Back to Index</a></button>
            <form method="POST" action="<?php echo $_SERVER['PHP_SELF'] ?>" enctype="multipart/form-data">
                <div class="mb-3">
                    <label>ID</label>
                    <input type="text" class="form-control" name="article_id" autocomplete="off" value="<?php echo (isset($dataGet) && $dataGet['article_id']) ? $dataGet['article_id'] : $_POST['article_id']; ?>" readonly>

                </div>
                <div class="mb-3">
                    <label>Title</label>
                    <input type="text" class="form-control" name="title" autocomplete="off" value="<?php echo (isset($dataGet) && $dataGet['title']) ? $dataGet['title'] : $_POST['title']; ?>">
                    <?php if (!empty($errors['title'])) : ?>
                        <div class="text-danger"><?php echo $errors['title']; ?></div>
                    <?php endif; ?>
                </div>
                <div class="mb-3">
                    <label>Description</label>
                    <input type="text" class="form-control" name="description" autocomplete="off" value="<?php echo (isset($dataGet) && $dataGet['description']) ? $dataGet['description'] : $_POST['description']; ?>">
                    <?php if (!empty($errors['description'])) : ?>
                        <div class="text-danger"><?php echo $errors['description']; ?></div>
                    <?php endif; ?>
                </div>
                <div class="mb-3">
                    <label>Categories_ID</label>
                    <select class="form-control" name="categories_id">
                        <?php
                        $sql = "SELECT categories_id,name FROM `categories` ORDER BY categories_id ASC";
                        $result = mysqli_query($con, $sql);

                        while ($row = mysqli_fetch_assoc($result)) {
                            echo '<option value="' . $row['categories_id'] . '">' . $row['name'] . '</option>';
                        }
                        ?>
                    </select>
                </div>
                <div class="mb-3">
                    <img style="height:100px;width: 100px" class="card-img-top prevew-img">
                    <input type="file" id="input_picture" name="picture" value="<?php echo isset($_SESSION['picture_article']) ? $_SESSION['picture_article'] : ''; ?>">
                    <?php if (isset($errors['picture'])) {
                        echo '<p style="color: red;">' . $errors['picture'] . '</p>';
                    } ?>
                </div>
                <div class="mb-4">
                    <label for="exampleFormControlTextarea1" class="form-label">Content</label>
                    <textarea class="form-control" name="content" id="content_edit" rows="4">
                <?php echo (isset($dataGet) && $dataGet['content']) ? $dataGet['content'] : $_POST['content'] ?>
              </textarea>
                    <?php if (!empty($errors['content'])) : ?>
                        <div class="text-danger"><?php echo $errors['content']; ?></div>
                    <?php endif; ?>
                </div>
                <button type="submit" class="btn btn-primary" name="submit" onclick="return confirm('Are you sure you want to update this article?');">Update Article</button>
            </form>
        </div>

    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script>
    <script src="https://cdn.tiny.cloud/1/v2actx1pn02mabdp87lonhsovy7lb90aca7wfhf3149o0eoa/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
        tinymce.init({
            image_dimensions: false,
            image_class_list: [{
                title: 'Responsive',
                value: 'img-responsive'
            }],
            image_description: true,
            image_caption: true,
            height: "600",
            selector: 'textarea#content_edit',
            plugins: 'code table lists image code ',
            toolbar: 'link image | code |image |undo redo | formatselect| bold italic | alignleft aligncenter alignright | indent outdent | bullist numlist | code | table',
            image_title: true,
            automatic_uploads: true,
            file_picker_types: 'image',
            file_picker_callback: (cb, value, meta) => {
                const input = document.createElement('input');
                input.setAttribute('type', 'file');
                input.setAttribute('accept', 'image/*');
                input.addEventListener('change', (e) => {
                    const file = e.target.files[0];
                    const reader = new FileReader();
                    reader.addEventListener('load', () => {
                        const id = 'blobid' + (new Date()).getTime();
                        const blobCache = tinymce.activeEditor.editorUpload.blobCache;
                        const base64 = reader.result.split(',')[1];
                        const blobInfo = blobCache.create(id, file, base64);
                        blobCache.add(blobInfo);
                        cb(blobInfo.blobUri(), {
                            title: file.name
                        });
                    });
                    reader.readAsDataURL(file);
                });
                input.click();
            },
            content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:16px }'
        });
    </script>
    <script>
        const inputImgEl = document.querySelector("#input_picture");
        const previewImgEl = document.querySelector(".prevew-img");
        let imgPreview = "<?php echo $link . $_SESSION['picture_article']; ?>";
        previewImgEl.setAttribute("src", imgPreview);
        const changeImg = (e) => {
            let file;
            let urlImg;
            if (e.target.files[0]) {
                file = e.target.files[0];
            }
            if (file) {
                urlImg = window.URL.createObjectURL(file);
                imgPreview = urlImg;
                previewImgEl.setAttribute("src", imgPreview);
            }
        }
        inputImgEl.addEventListener("change", changeImg);
    </script>
</body>

</html>
</body>

</html>