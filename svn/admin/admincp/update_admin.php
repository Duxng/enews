<?php
include('../operate.php');

if (!isset($_SESSION['login'])) {
    header('location: ../login.php');
    exit;
}

$errors = array();
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (empty($_POST['name'])) {
        $errors['name'] = "Không được để trống tên";
    } elseif (!preg_match('/^[\p{L}\s]+$/u', $_POST['name'])) {
        $errors['name'] = "Tên danh mục chỉ được nhập chữ cái và khoảng trắng.";
    }
    if (empty($_POST['status'])) {
        $errors['status'] = "Không được để trống status";
    } elseif (!is_numeric($_POST['status'])) {
        $errors['status'] = "Status chỉ được nhập số.";
    }
    if (empty($_POST['password'])) {
        $errors['password'] = "Mật khẩu không được để trống.";
    }
    if (empty($errors)) {
        update_admin($_POST);
    }
} else {
    if (isset($_GET['update_id'])) {
        $admin_id = $_GET['update_id'];
        $admin = get_id_Admin($admin_id);
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Update Admin</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/dashboard.css">
</head>

<body>
    <div class="container-fluid px-0 h-100vh row mt-5">
        <div class="col-2" style="height: auto; background-color: rgba(113, 99, 186, 255);">
            <?php include('../dashboard.php') ?>
        </div>
        <div class="col-10">
            <button class="btn btn-primary mb-2"><a href="admin_list.php" class="text-light">Back to Index</a></button>
            <form method="POST" action="<?php echo $_SERVER['PHP_SELF'] ?>">
                <div class="mb-3">
                    <label>ID</label>
                    <input type="text" class="form-control" name="admin_id" autocomplete="off" value="<?php echo (isset($admin) && $admin['admin_id']) ? $admin['admin_id'] : $_POST['admin_id']; ?>" readonly>

                </div>
                <div class="mb-3">
                    <label>Name</label>
                    <input type="text" class="form-control" name="name" autocomplete="off" value="<?php echo (isset($admin) && $admin['name']) ? $admin['name'] : $_POST['name'] ?>">
                    <?php if (!empty($errors['name'])) : ?>
                        <div class="text-danger"><?php echo $errors['name'] ?></div>
                    <?php endif; ?>
                </div>
                <div class="mb-3">
                    <label>Password</label>
                    <input type="text" class="form-control" name="password" autocomplete="off" value="<?php echo (isset($admin) && $admin['password']) ? $admin['password'] : $_POST['password'] ?>">
                    <?php if (!empty($errors['password'])) : ?>
                        <div class="text-danger"><?php echo $errors['password'] ?></div>
                    <?php endif; ?>
                </div>
                <select class="form-control" name="role_id">
                    <?php
                    $sql = "SELECT role_id,name FROM `roles` ORDER BY role_id ASC";
                    $result = mysqli_query($con, $sql);

                    while ($row = mysqli_fetch_assoc($result)) {
                        echo '<option value="' . $row['role_id'] . '">' . $row['name'] . '</option>';
                    }
                    ?>
                </select>
                <div class="mb-3">
                    <label>Status</label>
                    <input type="text" class="form-control" name="status" autocomplete="off" value="<?php echo (isset($admin) && $admin['status']) ? $admin['status'] : $_POST['status'] ?>">
                    <?php if (!empty($errors['status'])) : ?>
                        <div class="text-danger"><?php echo $errors['status'] ?></div>
                    <?php endif; ?>
                </div>
                <button type="submit" class="btn btn-primary" name="submit" onclick="return confirm('Are you sure you want to update this admin?');">Update Admin</button>
            </form>
        </div>

    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script>
</body>

</html>
</body>

</html>