<?php
include('../operate.php');
if (!isset($_SESSION['login'])) {
    header('location: ../login.php');
    exit;
}
$errors = array();
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (empty($_POST['name'])) {
        $errors['name'] = "Không được để trống tên";
    } elseif (!preg_match('/^[\p{L}\s]+$/u', $_POST['name'])) {
        $errors['name'] = 'Tên chỉ được nhập chữ cái và khoảng trắng.';
    }
    $existTingAdmin = get_all_admin();
    foreach ($existTingAdmin as $admin) {
        if ($admin['name'] === $_POST['name']) {
            $errors['name'] = 'Admin đã tồn tại';
            break;
        }
    }
    if (empty($_POST['password'])) {
        $errors['password'] = "Chưa nhập mật khẩu.";
    } elseif (mb_strlen($_POST["password"]) <= 8) {
        $errors['password'] = "Mật khẩu chứa ít nhất 8 kí tự !";
        // } elseif (!preg_match("#[0-9]+#", $password)) {
        //     $errors['password'] = "Mật khẩu chứa ít nhất một số !";
        // } elseif (!preg_match("#[A-Z]+#", $password)) {
        //     $errors['password'] = "Mật khẩu phải có một kí tự viết hoa !";
        // } elseif (!preg_match("#[a-z]+#", $password)) {
        //     $errors['password'] = "Mật khẩu phải có kí tự viết thường !";
        // } elseif (!preg_match("#[\W]+#", $password)) {
        //     $errors['password'] = "Mật khẩu phải chứa kí tự đặc biệt !";
        // } elseif (strcmp($password, $cpassword) !== 0) {
        //     $errors['password'] = "Passwords must match!";
    }
    if (empty($errors)) {
        add_Admin($_POST);
    }
}

?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Create New Role</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/dashboard.css">
</head>

<body>
    <div class="container-fluid px-0 h-100vh row mt-5">
        <div class="col-2" style="height: auto; background-color: rgba(113, 99, 186, 255);">
            <?php include('../dashboard.php') ?>
        </div>
        <div class="col-10">
            <button class="btn btn-primary mb-2"><a href="admin_list.php" class="text-light">Back to Index</a></button>
            <form method="POST" action="<?php echo $_SERVER['PHP_SELF'] ?>">
                <div class="mb-3">
                    <label>Admin name</label>
                    <input type="text" class="form-control" placeholder="Enter Admin name" name="name" autocomplete="off" value="<?php echo  isset($_POST['name']) ? $_POST['name'] : "" ?>">
                    <?php if (!empty($errors['name'])) : ?>
                        <div class="text-danger"><?php echo $errors['name']; ?></div>
                    <?php endif; ?>
                </div>
                <div class="mb-3">
                    <label>Password</label>
                    <input type="password" class="form-control" placeholder="Enter password" name="password" autocomplete="off">
                    <?php if (!empty($errors['password'])) : ?>
                        <div class="text-danger"><?php echo $errors['password']; ?></div>
                    <?php endif; ?>
                </div>
                <select class="form-control" name="role_id">
                    <?php
                    $sql = "SELECT role_id, name FROM `roles` ORDER BY role_id ASC";
                    $result = mysqli_query($con, $sql);

                    while ($row = mysqli_fetch_assoc($result)) {
                        echo '<option value="' . $row['role_id'] . '">' . $row['name'] . '</option>';
                    }
                    ?>
                </select>
                <button type="submit" class="btn btn-primary mt-4" name="submit">Submit</button>
            </form>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script>
</body>

</html>