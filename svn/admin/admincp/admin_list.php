<?php
include('../operate.php');
if (!isset($_SESSION['login'])) {
    header('location: ../login.php');
    exit;
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin list</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/dashboard.css">
</head>

<body>
    <div class="container-fluid px-0 h-100vh mt-5 row">
        <div class="col-2" style="height: auto; background-color: rgba(113, 99, 186, 255);">
            <?php include('../dashboard.php') ?>
        </div>
        <div class="col-10">
            <button class="btn btn-primary mb-2"><a href="add_admin.php" class="text-light">Add Admin</a></button>
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">Admin_ID</th>
                        <th scope="col">Name</th>
                        <th scope="col">Password</th>
                        <th scope="col">Created_at</th>
                        <th scope="col">Updated_at</th>
                        <th scope="col">Role</th>
                        <th scope="col">Status</th>
                        <th scope="col">Edit</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $result = get_list_Admin();
                    while ($row = mysqli_fetch_assoc($result)) : ?>
                        <tr>
                            <td><?php echo $row['admin_id']; ?></th>
                            <td><?php echo $row['name']; ?></td>
                            <td><?php echo $row['password']; ?></td>
                            <td><?php echo $row['created_at']; ?></td>
                            <td><?php echo $row['updated_at'] ?></td>
                            <td><?php echo $row['role']; ?></td>
                            <td><?php echo $row['status']; ?></td>
                            <td>
                                <button class="btn btn-primary"><a href="update_admin.php?update_id=<?php echo $row['admin_id']; ?>" class="text-light">Update</a></button>
                                <button class="btn btn-danger"><a href="delete_admin.php?delete_id=<?php echo $row['admin_id']; ?>" class="text-light">Delete</a></button>
                            </td>
                        </tr>
                    <?php
                    endwhile
                    ?>
                </tbody>
            </table>
        </div>

    </div>
</body>

</html>