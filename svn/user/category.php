<?php
include('operate.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Danh Mục Bài Báo</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css" />
</head>

<body>
    <div class="header">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <ul class="header_list">
                        <a href="index.php">Trang Chủ</a>
                        <?php
                        $sql = "SELECT * FROM categories ORDER BY categories_id DESC";
                        $result = mysqli_query($con, $sql);
                        while ($row = mysqli_fetch_assoc($result)) : ?>
                            <a href="category.php?id=<?php echo $row['categories_id']; ?>">
                                <li><?php echo $row['name']; ?></li>
                            </a>
                        <?php
                        endwhile
                        ?>
                        <form action="search.php" method="get" class="ml-3">
                            <input type="text" name="search" placeholder="Tìm Kiếm" value="">
                            <button class="btn btn-dark">Search</button>
                        </form>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="pt-5" style="background-color: #f1f1f1;">
        <div class="container article-container">
            <div class="row" style="background-color: white;">
                <div class="col-8">
                    <h1 class="category-title mt-4">Tin tức theo danh mục</h1>
                    <?php
                    $link = '../admin/uploads/';
                    if (isset($_GET['id'])) {
                        $category_id = $_GET['id'];
                        $sql = "SELECT * FROM articles WHERE categories_id = $category_id ORDER BY updated_at DESC";
                        $result = mysqli_query($con, $sql);
                        while ($row = mysqli_fetch_assoc($result)) : ?>
                            <div class="post-item">
                                <a href="detail.php?id=<?php echo $row['article_id'] ?>"><img src="../uploads/<?php echo $link . $row['picture'] ?>" alt="" class="item-img"></a>
                                <div style="padding-left:16px;">
                                    <a href="detail.php?id=<?php echo $row['article_id'] ?>" class="">
                                        <h6>
                                            <?php echo $row['title'] ?>
                                        </h6>
                                    </a>
                                    <p style="height: 100px;" class="show-content"><?php echo $row['description'] ?> </p>
                                </div>
                            </div>
                            <hr>
                    <?php
                        endwhile;
                    }
                    ?>
                </div>
                <div class="col-4">
                </div>
            </div>
        </div>
    </div>
    <footer class="mt-4 bg-dark text-center text-white">
        <div class="container p-4 pb-0">
            <section class="mb-4">
                <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"><i class="fab fa-facebook-f"></i></a>
                <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"><i class="fab fa-twitter"></i></a>
                <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"><i class="fab fa-google"></i></a>
                <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"><i class="fab fa-instagram"></i></a>
                <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"><i class="fab fa-linkedin-in"></i></a>
                <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"><i class="fab fa-github"></i></a>
            </section>
        </div>
        <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
            © 2020 Copyright:
            <a class="text-white" href="https://mdbootstrap.com/">MDBootstrap.com</a>
        </div>
    </footer>
</body>

</html>