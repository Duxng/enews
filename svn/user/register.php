<?php
include('operate.php');
$errors = array();
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (empty($_POST['user_name'])) {
        $errors['user_name'] = "User Name is required.";
    }
    if (empty($_POST['full_name'])) {
        $errors['full_name'] = "Full Name is required.";
    } elseif (!preg_match("/^[a-zA-Z ]*$/", $_POST['full_name'])) {
        $errors['full_name'] = "Only letters and white space allowed in Full Name.";
    }
    if (empty($_POST['phone'])) {
        $errors['phone'] = "Phone Number is required.";
    } elseif (!preg_match("/^[0-9]*$/", $_POST['phone'])) {
        $errors['phone'] = "Phone Number should contain only numbers.";
    }
    if (empty($_POST['email'])) {
        $errors['email'] = "Email Address is required.";
    } elseif (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
        $errors['email'] = "Invalid email format.";
    }
    if (empty($_POST['password'])) {
        $errors['password'] = "Password is required.";
    }
    if (empty($_POST['repeat_password'])) {
        $errors['repeat_password'] = "Repeat Password is required.";
    }
    if ($_POST['password'] !== $_POST['repeat_password']) {
        $errors['password'] = "Passwords do not match.";
    }
    if (empty($errors)) {
        createUser($_POST);
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Register</title>
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="css/userstyle.css">
</head>

<body>
    <div class="container wrapper">
        <div class="row">
            <div class="col-5 image">
                <img src="images/login.jpg" width="100%" height="100%">
            </div>
            <div class="col-7 form">
                <div class="text-center">
                    <h1 class="h4">Create an Account!</h1>
                </div>
                <form class="user" autocomplete="off" method="POST" action="<?php echo $_SERVER['PHP_SELF'] ?>">
                    <div class="form-group row ">
                        <div class="col-6 ">
                            <input type="text" name="user_name" class="form-control  " placeholder="User Name">
                            <?php
                            if (!empty($errors['user_name'])) : ?>
                                <div class="text-danger"><?php echo $errors['user_name']; ?></div>
                            <?php endif; ?>
                        </div>
                        <div class="col-6">
                            <input type="text" name="full_name" class="form-control " placeholder="Full Name">
                            <?php
                            if (!empty($errors['full_name'])) : ?>
                                <div class="text-danger"><?php echo $errors['full_name']; ?></div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="tel" name="phone" class="form-control mt-4" placeholder="Phone">
                        <?php
                        if (!empty($errors['phone'])) : ?>
                            <div class="text-danger"><?php echo $errors['phone']; ?></div>
                        <?php endif; ?>
                    </div>
                    <div class="form-group">
                        <input type="email" name="email" class="form-control  mt-4" placeholder="Email Address">
                        <?php
                        if (!empty($errors['email'])) : ?>
                            <div class="text-danger"><?php echo $errors['email']; ?></div>
                        <?php endif; ?>
                    </div>
                    <div class="form-group row">
                        <div class="col-6 ">
                            <input type="password" name="password" class="form-control  mt-4" placeholder="Password">
                            <?php
                            if (!empty($errors['password'])) : ?>
                                <div class="text-danger"><?php echo $errors['password']; ?></div>
                            <?php endif; ?>
                        </div>
                        <div class="col-6">
                            <input type="password" name="repeat_password" class="form-control mt-4" placeholder="Repeat Password">
                            <?php
                            if (!empty($errors['repeat_password'])) : ?>
                                <div class="text-danger"><?php echo $errors['repeat_password']; ?></div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <input type="submit" class="btn btn-primary mt-4" value="Register Account">
                </form>
                <hr>
                <div class="text-center mt-2">
                    <a class="" href="login.php">Already have an account? Login!</a>
                </div>
            </div>

        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

</body>

</html>