<?php
include('operate.php');
$errors = array();
if (isset($_POST['login'])) {
    $email = $_POST['email'];
    $password = $_POST['password'];
    if (empty($email)) {
        $errors['email'] = "Email is required.";
    } elseif (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
        $errors['email'] = "Invalid email format.";
    }
    if (empty($password)) {
        $errors['password'] = "Password is required.";
    }
    if (empty($errors)) {
        $sql = "SELECT email, password FROM users WHERE email='$email' AND password='$password'";
        $result = mysqli_query($con, $sql);
        $count = mysqli_num_rows($result);
        if ($count > 0) {
            header('location: ../index.php');
        } else {
            header('location: login.php');
        }
    }
    
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="css/userstyle.css">
</head>

<body>
    <div class="container wrapper">
        <div class="row">
            <div class="col-6 image">
                <img src="images/login.jpg" width="100%" height="100%" alt="">
            </div>
            <div class="col-6 form">
                <div class="text-center">
                    <h1 class="h4 mt-4">Welcome Back!</h1>
                </div>
                <form class="user" method="POST" autocomplete="off">
                    <div class="form-group mt-4">
                        <input type="email" name="email" class="form-control form-control-user" placeholder="Enter Email Address..." value="<?php echo isset($_POST['email']) ? $_POST['email'] : "" ?>">
                        <?php
                        if (!empty($errors['email'])) : ?>
                            <div class="text-danger"><?php echo $errors['email']; ?></div>
                        <?php endif; ?>
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" class="form-control form-control-user mt-4" placeholder="Password">
                        <?php
                        if (!empty($errors['password'])) : ?>
                            <div class="text-danger"><?php echo $errors['password']; ?></div>
                        <?php endif; ?>
                    </div>
                    <input type="submit" name="login" class=" mt-4 btn btn-primary" value="Login">
                </form>
                <hr>
                <div class="text-center mt-2">
                    <a class="" href="register.php">Create an Account !</a>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>

</html>