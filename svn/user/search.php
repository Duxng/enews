<?php
include('operate.php');
if (isset($_GET['search'])) {
    $name = $_GET['search'];
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Search</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css" />
</head>

<body>
    <div class="header">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <ul class="header_list">
                        <a href="index.php">Trang Chủ</a>
                        <?php
                        $sql = "SELECT * FROM categories ORDER BY categories_id DESC";
                        $result = mysqli_query($con, $sql);
                        while ($row = mysqli_fetch_assoc($result)) : ?>
                            <a href="category.php?id=<?php echo $row['categories_id']; ?>">
                                <li><?php echo $row['name']; ?></li>
                            </a>
                        <?php
                        endwhile;
                        ?>
                        <form action="<?php $_SERVER['PHP_SELF'] ?>" method="get" class="ml-3">
                            <input type="text" name="search" placeholder="Tìm Kiếm" value="<?php echo $name ?>">
                            <button class="btn btn-dark">Search</button>
                        </form>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="pt-5" style="background-color: #f1f1f1;">
        <div class="container article-container">
            <div class="row" style="background-color: white;">
                <div class="col-8">
                    <?php
                    $link = '../admin/uploads/';
                    if (isset($_GET['search'])) {
                        $search_query = $_GET['search'];
                        $search_results = perform_search($search_query);
                        while ($row = mysqli_fetch_assoc($search_results)) : ?>
                            <div class="post-item">
                                <a href="detail.php?id=<?php echo $row['article_id'] ?>"><img src="../uploads/<?php echo $link . $row['picture'] ?>" alt="" class="item-img"></a>
                                <div style="padding-left:16px;">
                                    <a href="detail.php?id=<?php echo $row['article_id'] ?>" class="">
                                        <h6>
                                            <?php echo $row['title'] ?>
                                        </h6>
                                    </a>
                                    <p style="height: 100px;" class="show-content"><?php echo $row['description'] ?> </p>
                                </div>
                            </div>
                            <hr>
                    <?php endwhile;
                    }
                    ?>
                </div>
                <div class="col-4">
                </div>
            </div>
        </div>
    </div>

</body>

</html>