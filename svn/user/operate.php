<?php
include('config/config.php');

function checkResult($result)
{
    global $con;
    if (isset($result)) {
        return $result;
    } else {
        echo mysqli_error($con);
        exit;
    }
}

function createUser($data)
{
    global $con;
    $user_name = $data['user_name'];
    $full_name = $data['full_name'];
    $phone = $data['phone'];
    $email = $data['email'];
    $password = $data['password'];
    $sql = "INSERT INTO `users`(
        `user_name`,
        `full_name`,
        `phone`,
        `email`,
        `password`
    ) 
        VALUES ('$user_name', '$full_name', '$phone', '$email', '$password')";
    $result = mysqli_query($con, $sql);
    if (checkResult($result)) {
        redirect('login.php');
    }
}

function get_name_Categories()
{
    global $con;
    $sql = "SELECT * FROM `categories`";
    $sql .= "ORDER BY categories_id";
    $result = mysqli_query($con, $sql);
    return checkResult($result);
}

function perform_search($query)
{
    global $con;
    $sql = "SELECT * FROM articles WHERE title LIKE '%$query%' OR description LIKE '%$query%'";
    $result = mysqli_query($con, $sql);

    return $result;
}


function perform_category($category_name)
{
    global $con;
    $category_name = mysqli_real_escape_string($con, $category_name);
    $sql = "SELECT * FROM articles 
            INNER JOIN categories ON articles.categories_id = categories.categories_id
            WHERE categories.name = '$category_name'
            ORDER BY articles.article_id DESC LIMIT 5";

    $result = mysqli_query($con, $sql);

    return $result;
}
