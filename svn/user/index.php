<?php
include('operate.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Enews</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css">
</head>

<body>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <div class="header">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <ul class="header_list">
                        <a href="index.php">Trang Chủ</a>
                        <?php
                        $sql = "SELECT * FROM categories ORDER BY categories_id DESC";
                        $result = mysqli_query($con, $sql);
                        while ($row = mysqli_fetch_assoc($result)) : ?>
                            <a href="category.php?id=<?php echo $row['categories_id']; ?>">
                                <li><?php echo $row['name']; ?></li>
                            </a>
                        <?php
                        endwhile
                        ?>
                        <form action="search.php" method="get" class="ml-3">
                            <input type="text" name="search" placeholder="Tìm Kiếm" value="">
                            <button type="submit" class="btn btn-dark">Search</button>
                        </form>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-8 ">
                <div class="row main_content">
                    <?php
                    $sql = "SELECT * FROM articles ORDER BY updated_at DESC LIMIT 8";
                    $result = mysqli_query($con, $sql);
                    if ($row = mysqli_fetch_assoc($result)) :
                    ?>
                        <div class="first_new col-8 mt-2">
                            <a href="detail.php?id=<?php echo $row['article_id']; ?>"><img src="../admin/uploads/<?php echo $row['picture']; ?>" alt="" class="first_new_img"></a>
                            <h3 class="tilte show-content-index">
                                <a href="detail.php?id=<?php echo $row['article_id']; ?>" class="first_new_title_link show-content-index"><?php echo $row['title']; ?></a>
                            </h3>
                            <p class="show-content-index">
                                <?php echo $row['description']; ?>
                            </p>
                        </div>
                    <?php endif; ?>
                    <?php
                    if ($row = mysqli_fetch_assoc($result)) :
                    ?>
                        <div class="second_new col-4 mt-2">
                            <a href="detail.php?id=<?php echo $row['article_id']; ?>" class=""><img width="533" height="300" src="../admin/uploads/<?php echo $row['picture']; ?>" alt="" class="second_new_img"></a>
                            <a href="">
                                <p class="secone_new_title show-content-index"><?php echo $row['title']; ?></p>
                            </a>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="row article_list">
                    <div class="col-4 article_list_item">
                        <div class="cover">
                            <?php
                            if ($row = mysqli_fetch_assoc($result)) :
                            ?>
                                <a href="detail.php?id=<?php echo $row['article_id']; ?>"><img src="../admin/uploads/<?php echo $row['picture']; ?>" alt="" class="article_list_item_img"></a>
                                <a href="detail.php?id=<?php echo $row['article_id']; ?>">
                                    <p class="tilte show-content-index"><?php echo $row['title']; ?></p>
                                </a>
                        </div>
                    <?php endif; ?>
                    </div>
                    <div class="col-4 article_list_item">
                        <div class="cover">
                            <?php
                            if ($row = mysqli_fetch_assoc($result)) :
                            ?>
                                <a href="detail.php?id=<?php echo $row['article_id']; ?>"><img src="../admin/uploads/<?php echo $row['picture']; ?>" alt="" class="article_list_item_img"></a>
                                <a href="detail.php?id=<?php echo $row['article_id']; ?>">
                                    <p class="tilte show-content-index"><?php echo $row['title']; ?></p>
                                </a>
                        </div>
                    <?php endif; ?>
                    </div>
                    <div class="col-4 article_list_item">
                        <div class="cover">
                            <?php
                            if ($row = mysqli_fetch_assoc($result)) :
                            ?>
                                <a href="detail.php?id=<?php echo $row['article_id']; ?>"><img src="../admin/uploads/<?php echo $row['picture']; ?>" class="article_list_item_img"></a>
                                <a href="detail.php?id=<?php echo $row['article_id']; ?>">
                                    <p class="tilte show-content-index"><?php echo $row['title']; ?></p>
                                </a>
                        </div>
                    <?php endif; ?>
                    </div>
                </div>
                <hr class="solid">
                <div class="row" style="margin-top: 2rem;">
                    <div class="col">
                        <div class="post-item">
                            <?php
                            if ($row = mysqli_fetch_assoc($result)) :
                            ?>
                                <img src="../admin/uploads/<?php echo $row['picture']; ?>" alt="" class="item-img">
                                <div style="padding-left: 16px;">
                                    <h6>
                                        <a href="detail.php?id=<?php echo $row['article_id']; ?>" class="show-content-index"><?php echo $row['title']; ?></a>
                                    </h6>
                                    <a href="">
                                        <p class="tilte show-content-index"><?php echo $row['title']; ?></p>
                                    </a>
                                </div>
                            <?php endif; ?>
                        </div>
                        <hr class="solid">
                        <div class="post-item">
                            <?php
                            if ($row = mysqli_fetch_assoc($result)) :
                            ?>
                                <img src="../admin/uploads/<?php echo $row['picture']; ?>" alt="" class="item-img">
                                <div style="padding-left: 16px;">
                                    <h6>
                                        <a href="detail.php?id=<?php echo $row['article_id']; ?>" class="show-content-index"><?php echo $row['title']; ?></a>
                                    </h6>
                                    <a href="">
                                        <p class="tilte show-content-index"><?php echo $row['title']; ?></p>
                                    </a>
                                </div>
                            <?php endif; ?>
                        </div>
                        <hr class="solid">
                        <div class="post-item">
                            <?php
                            if ($row = mysqli_fetch_assoc($result)) :
                            ?>
                                <img src="../admin/uploads/<?php echo $row['picture']; ?>" alt="" class="item-img">
                                <div style="padding-left: 16px;">
                                    <h6>
                                        <a href="detail.php?id=<?php echo $row['article_id']; ?>" class="show-content-index"><?php echo $row['title']; ?></a>
                                    </h6>
                                    <a href="">
                                        <p class="tilte show-content-index"><?php echo $row['title']; ?></p>
                                    </a>
                                </div>
                            <?php endif; ?>
                        </div>
                        <hr class="solid">
                        <div class="slider-content row">
                            <div class="category-slider-header col-12">
                                <h3>Thế Giới</h3>
                            </div>
                            <div class="col-12">
                                <div class="MultiCarousel" data-items="1,3,5,6" data-slide="1" id="MultiCarousel" data-interval="1000" style="padding: 0">
                                    <div class="MultiCarousel-inner">
                                        <?php
                                        $category_name = 'Thế Giới';
                                        $articles_in_category = perform_category($category_name);

                                        while ($row = mysqli_fetch_assoc($articles_in_category)) :
                                        ?>
                                            <div class="item slider-content-box">
                                                <div class="pad15">
                                                    <div class="slider-item">
                                                        <a href="detail.php?id=<?php echo $row['article_id']; ?>" class="post_link">
                                                            <img class="slider-pc-content__img" src="../admin/uploads/<?php echo $row['picture']; ?>" alt="">
                                                            <h4 class="lead slider-item-title text-wrap-title" style="height: 65px"><?php echo $row['title']; ?></h4>
                                                        </a>
                                                        <p class="slider-item-desc text-wrap-desc">
                                                            <?php echo $row['description']; ?>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endwhile; ?>
                                    </div>
                                    <button class="btn btn-primary leftLst">
                                        &lt;
                                    </button>
                                    <button class="btn btn-primary rightLst">&gt;</button>
                                </div>
                            </div>
                        </div>
                        <hr class="solid">
                        <div class="slider-content row">
                            <div class="category-slider-header col-12">
                                <h3>Khoa học</h3>
                            </div>
                            <div class="col-12">
                                <div class="MultiCarousel" data-items="1,3,5,6" data-slide="1" id="MultiCarousel" data-interval="1000" style="padding: 0">
                                    <div class="MultiCarousel-inner">
                                        <?php
                                        $category_name = 'Khoa học';
                                        $articles_in_category = perform_category($category_name);

                                        while ($row = mysqli_fetch_assoc($articles_in_category)) :
                                        ?>
                                            <div class="item slider-content-box">
                                                <div class="pad15">
                                                    <div class="slider-item">
                                                        <a href="detail.php?id=<?php echo $row['article_id']; ?>" class="post_link">
                                                            <img class="slider-pc-content__img" src="../admin/uploads/<?php echo $row['picture']; ?>" alt="">
                                                            <h4 class="lead slider-item-title text-wrap-title" style="height: 65px"><?php echo $row['title']; ?></h4>
                                                        </a>
                                                        <p class="slider-item-desc text-wrap-desc">
                                                            <?php echo $row['description']; ?>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endwhile; ?>
                                    </div>
                                    <button class="btn btn-primary leftLst">
                                        &lt;
                                    </button>
                                    <button class="btn btn-primary rightLst">&gt;</button>
                                </div>
                            </div>
                        </div>
                        <hr class="solid">
                        <div class="slider-content row">
                            <div class="category-slider-header col-12">
                                <h3>Thời sự</h3>
                            </div>
                            <div class="col-12">
                                <div class="MultiCarousel" data-items="1,3,5,6" data-slide="1" id="MultiCarousel" data-interval="1000" style="padding: 0">
                                    <div class="MultiCarousel-inner">
                                        <?php
                                        $category_name = 'Thời sự';
                                        $articles_in_category = perform_category($category_name);

                                        while ($row = mysqli_fetch_assoc($articles_in_category)) :
                                        ?>
                                            <div class="item slider-content-box">
                                                <div class="pad15">
                                                    <div class="slider-item">
                                                        <a href="detail.php?id=<?php echo $row['article_id']; ?>" class="post_link">
                                                            <img class="slider-pc-content__img" src="../admin/uploads/<?php echo $row['picture']; ?>" alt="">
                                                            <h4 class="lead slider-item-title text-wrap-title" style="height: 65px"><?php echo $row['title']; ?></h4>
                                                        </a>
                                                        <p class="slider-item-desc text-wrap-desc">
                                                            <?php echo $row['description']; ?>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endwhile; ?>
                                    </div>
                                    <button class="btn btn-primary leftLst">
                                        &lt;
                                    </button>
                                    <button class="btn btn-primary rightLst">&gt;</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-4 mt-2">
                <img src="images/003.jpg" alt="" style="width:342.62px; height:507.58px">
                <div class="mt-4">
                    <h4 class="mb-1 title-side">Thời Trang</h4>
                </div>
                <?php
                $category_name = 'Thời Trang';
                $articles_in_category = perform_category($category_name);

                while ($row = mysqli_fetch_assoc($articles_in_category)) :
                ?>
                    <div class="d-flex">
                        <img src="../admin/uploads/<?php echo $row['picture']; ?>" alt="" class="item-img-side">
                        <div style="padding-left: 1rem; line-height: 18px;">
                            <a href="detail.php?id=<?php echo $row['article_id']; ?>" class="">
                                <h6>
                                    <?php echo $row['title']; ?>
                                </h6>
                            </a>
                            <p class="pt-1 mb-1" style="font-size: small;">
                                <?php echo $row['created_at']; ?>
                            </p>
                        </div>
                    </div>
                <?php endwhile; ?>
                <img src="images/009.jpg" alt="" style="width:342.62px; height:507.58px ">
                <div class="mt-4">
                    <h4 class="mb-1 title-side">Điện Ảnh</h4>
                </div>
                <?php
                $category_name = 'Điện Ảnh';
                $articles_in_category = perform_category($category_name);

                while ($row = mysqli_fetch_assoc($articles_in_category)) :
                ?>
                    <div class="d-flex">
                        <img src="../admin/uploads/<?php echo $row['picture']; ?>" alt="" class="item-img-side">
                        <div style="padding-left: 1rem; line-height: 18px;">
                            <a href="detail.php?id=<?php echo $row['article_id']; ?>" class="">
                                <h6>
                                    <?php echo $row['title']; ?>
                                </h6>
                            </a>
                            <p class="pt-1 mb-1" style="font-size: small;">
                                <?php echo $row['created_at']; ?>
                            </p>
                        </div>
                    </div>
                <?php endwhile; ?>
                <img class="mt-4" src="images/010.jpg" alt="" style="width:342.62px; height:507.58px ">
                <img class="mt-4" src="images/012.jpg" alt="" style="width:342.62px; height:507.58px ">
            </div>
        </div>
    </div>
    <footer class="mt-4 bg-dark text-center text-white">
        <div class="container p-4 pb-0">
            <section class="mb-4">
                <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"><i class="fab fa-facebook-f"></i></a>
                <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"><i class="fab fa-twitter"></i></a>
                <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"><i class="fab fa-google"></i></a>
                <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"><i class="fab fa-instagram"></i></a>
                <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"><i class="fab fa-linkedin-in"></i></a>
                <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"><i class="fab fa-github"></i></a>
            </section>
        </div>
        <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
            © 2020 Copyright:
            <a class="text-white" href="https://mdbootstrap.com/">MDBootstrap.com</a>
        </div>
    </footer>

</body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.min.js"></script>

<script src="js/slider.js"></script>

</html>